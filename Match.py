__author__ = 'david'

from datetime import datetime

NOTHING = -1
SCISSORS = 0
PAPER = 1
ROCK = 2

def ssp_inv(arg):
    if(arg == SCISSORS):
        return ROCK
    if(arg == PAPER):
        return SCISSORS
    return PAPER

class Match:
    def __init__(self, a, b):
        self.k1 = a
        self.k2 = b
        self.participants = {a: -1, b: -1}
        assert len(self.participants) == 2
        self.started = False
        self.ended = False
        self.round = 0
        self.proposed_at = datetime.now()

    def choose(self, id, what):
        assert what in [PAPER, SCISSORS, ROCK]
        self.participants[id] = what

    def next_round(self):
        self.round += 1
        for k in self.participants.keys():
            self.participants[k] = NOTHING

    def round_ended(self):
        for k in self.participants.keys():
            if self.participants[k] == NOTHING:
                return False
        return True

    def status(self, myid):
        if(not self.round_ended()):
            return 'undecided'
        else:
            if self.participants[self.k1] == self.participants[self.k2]:
                return 'patt'
            else:
                inv = ssp_inv(self.participants[self.k1])
                if(inv == self.participants[self.k2]):
                    if(myid == self.k1):
                        return 'lost'
                    return 'won'
                if(myid == self.k1):
                    return 'won'
                return 'lost'

