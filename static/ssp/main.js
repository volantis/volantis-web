/**
 * Created by jonasfelber on 04/10/15.
 */

SCISSORS = 0;
PAPER = 1;
ROCK = 2;

var game_ended = false;

function checkWin(){
    if(game_ended) {
        return;
    }
    $.ajax('/game',{
        success: function(res) {
            if(res != 'undecided') {
                game_ended = true;
                $('.selections').html(res);
                if(res == 'won') {
                    $('.selections').html(res+ "<br>generating a voucher for you!");
                    setTimeout(canIhazVoucher, 1000)
                } else if(res == 'patt') {
                    $('.selections').html(res+ "<br>generating a voucher for you!");
                    setTimeout(canIhazVoucher, 1000)
                }
            }
        }
    });
    setTimeout(checkWin, 500);
}

function canIhazVoucher(){
    window.location.href = '/canihazvoucher';
}

$(document).ready(function(){
    $(".selections div").on('click', function(event){
        var type = $(event.target).data('type');
        var num = -1;
        if(type == 'rock') {
            num = ROCK
        } else if(type == 'paper') {
            num = PAPER
        } else if(type == 'scissors') {
            num = SCISSORS
        } else {
            num = -1
        }
        $.ajax('/game/' + num);
    });
    setTimeout(checkWin, 100);
});