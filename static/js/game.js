$(document).ready(init);

function init() {
	canvas = document.getElementById('canvas');
	canvas.addEventListener('mouseup', mouseUp, false);
	canvas.addEventListener('mousemove', mouseDrag, false);
	ctx = canvas.getContext('2d');
	Config = {
		ballRad: 10,
		height: canvas.height,
		width: canvas.width,
		blockHeight: 10,
		blockWidth: 30,
		timeout: 15,
		padWidth: 30
	};

	ball = {
		x: Config.width/2 - Config.ballRad,
		y: Config.height - 2 * Config.ballRad,
		angle: 0,
		speedX: 0,
		speedY: 0,
		onpad: true,
		speed: 1/3
	};

	pad = {
		x: Config.width/2,
		y: Config.height - 10
	}
	genData();
	loop();
}

function mouseUp(event) {
	var x = event.layerX;
	pad.x = x;
	if(ball.onpad) {
		ball.x = x;
		ball.y = pad.y - Config.ballRad - 1;
		ball.onpad = false;
		ball.speedY = -1;
		ball.speedX = 0.1
	}
}

function mouseDrag(event) {
	var x = event.layerX;
	pad.x = x;
	if(ball.onpad) {
		ball.x = x;
	}
}

function loop() {
	render(Config.timeout);
	setTimeout(loop, Config.timeout);
}

// render

function render(dt) {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	renderPad();
	renderBall(dt);
	data.forEach(renderBlock);
}

function renderBall(dt) {
	ball.x += dt * ball.speedX * ball.speed;
	ball.y += dt * ball.speedY * ball.speed;
	ctx.beginPath();
	ctx.arc(ball.x, ball.y, Config.ballRad, 0, 2 * Math.PI,false);
	ctx.fill();	
	checkHit();
}

function renderPad() {
	var box = padBox();
	ctx.fillRect(box.x, box.y, box.width, box.height);
}

function renderBlock(block) {
	ctx.fillStyle = "rgb(200,0,0)";
	ctx.fillRect (block.x * 30, block.y * 10, 25, 5);
}

function checkHit(){
	checkWallHit();
	checkBlockHit();
	checkPaddingHit();
	checkBottomHit();
}

function checkWallHit() {
	var ballB = ballBox(); 
	var wall1 = {
		x: 0,
		y: 0, height: Config.height, width: 1
	}

	if(intersect(wall1, ballB)) {
		ball.speedX = -ball.speedX;
		console.log("hit left");
	}
	wall1.x = Config.width;
	if(intersect(wall1, ballB)) {
		ball.speedX = -ball.speedX;
		console.log("hit right");
	}
	wall3 = {
		x: 0,
		y: 0,
		height: 1,
		width: Config.width
	};
	if(intersect(wall3, ballB)) {
		ball.speedY = -ball.speedY;
		console.log("Hit top");
	}
}

function checkBlockHit() {

}

function checkPaddingHit() {
	var box = padBox();
	var ballB = ballBox();
	if(intersect(box, ballB)) {
		ball.speedY = -ball.speedY;
		var dx = pad.x - ball.x;
		var dy = pad.y - ball.y;
		var norm = Math.sqrt(Math.pow(dx,2) + Math.pow(dy, 2));
		dx = dx/norm;
		dy = dy/norm;
		ball.speedX = -dx;
		ball.speedY = -Math.abs(dy);
		ball.speed *= 1.1;
		console.log("hit pad");
	}
}

function checkBottomHit() {

}

// util

function padBox() {
	return {
		x: pad.x - Config.padWidth/2,
		y: pad.y,
		width: Config.padWidth,
		height: 3
	}
}

function ballBox() {
	return {
		x: ball.x - Config.ballRad,
		y: ball.y - Config.ballRad,
		height: 2 * Config.ballRad,
		width: 2 * Config.ballRad
	}
}

function brickBox(brick) {
	return {
		x: brick.x * Config.blockWidth,
		y: brick.y * Config.blockHeight,
		height: Config.blockHeight - 1,
		width: Config.blockWidth - 1
	}
}

function intersect(r1, r2) {
	return !(r2.x > r1.x + r1.width || 
			r2.x + r2.width < r1.x || 
			r2.y > r1.y + r1.height ||
			r2.y + r2.height < r1.y);
}

// dummy

function genData() {
	data = [];
	for(i = 0; i < 10; i++) {
		for(j = 0; j < 3; j++) {
			data.push({
				x: i,
				y: j
			});
		}
	}
}
