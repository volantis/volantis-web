__author__ = 'david'

import config
from suds.client import Client


class AxiomConnection:
    def __init__(self):
        try:
            c = Client(config.ACCESS_URL)
            self._access = c
            self.Aservice = c.service
            self.Afactory = c.factory
            c = Client(config.VOUCHER_URL)
            self._voucher = c
            self.Vservice = c.service
            self.Vfactory = c.factory
        except:
            raise EnvironmentError("Can't initialize AxiomConnection")

    def login(self, email):
        login_parameters = self.Afactory.create("ns30:customerLoginParameters")
        login_parameters = self._set_defaults(login_parameters)
        login_parameters = self._set_user_details_from_email(login_parameters, email)
        response = self.Aservice.customerLogin(login_parameters)
        user = response.customerLoginOutputs
        user_loyalty = response.loyaltyAccountSet
        return user, user_loyalty

    def getVoucher(self, tier, user):
        # first we trigger the appropriate campaign
        sa_params = self._create_sales_advice_parameters()
        sa_params = self._set_user_details_from_card(sa_params, user[1][0][0].relationshipNo)
        if tier == 1:
            sa_params.transactionType = "L1"
        elif tier == 2:
            sa_params.transactionType = "L2"
        elif tier == 3:
            sa_params.transactionType = "L3"
        else:
            raise ValueError("Not applicable tier requested")
        print(sa_params)
        r = self.Vservice.salesAdvice(sa_params)
        if not str(0) == r.returnMessageOutput.responseCode:
            print(r)
            raise EnvironmentError("salesAdvice funked up")
        # then we get the actual voucher
        cl_params = self._create_campaign_list_parameters(user[0].externalCustomerNo)
        r = self.Aservice.getCampaignList(cl_params)
        if not str(0) == r.returnMessageOutput.responseCode:
            raise EnvironmentError("getCampaignList funked up")
        l = len(r.campaignSet[0])
        return r.campaignSet[0][l-1]

    def _set_defaults(self, params):
        params.institutionID = config.INSTITUTION_ID
        params.institutionPassword = config.INSTITUTION_PW
        params.responseLanguage = "en"
        return params

    def _set_user_details_from_email(self, params, email):
        params.instrumentNo = email
        params.instrumentType = "E"
        params.password = config.PW
        return params

    def _set_user_details_from_card(self, params, card):
        params.instrumentNo = card
        params.instrumentType = "R"
        params.customerPassword = config.PW
        return params

    def _create_sales_advice_parameters(self):
        p = self.Vfactory.create("ns3:salesRequestAdviceRefundCompletionParameters")
        p = self._set_defaults(p)
        p.transactionCategory = "BNS"
        p.transactionChannel = "APP"
        p.acquirerId = "12322500000"
        p.merchantId = "V1"
        p.terminalId = "TV1"
        p.terminalBatchNumber = 1
        p.amount = 0
        return p

    def _create_campaign_list_parameters(self, external_customer_no):
        p = self.Afactory.create("ns30:getCampaignListParameters")
        p = self._set_defaults(p)
        p.userProfile = "APPUSER"
        p.sortKeyName = "EXPIRATION_DATE"
        p.sortKeyDir = "A"
        p.forceCount = "N"
        p.recordType = "CAMP"
        p.campaignTypeList = "DERV"
        p.campaignStatusList = "A"
        p.customerSpec = external_customer_no
        p.sourceChannel = "APP"
        p.runOption = "C"
        return p
