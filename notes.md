# Server Documentation

## API

Clients open URL "/discovery" and POST a JSON document in the following format:

```
{
id: UUID,
discovery_id: UUID
}
```

client:

socket = new WebSocket('ws:///127.0.0.1:9000/ws')
socket.onmessage = function(a){console.log(a)}
socket.send("hello server?") // or JSON.stringify(obj)

https://ws4py.readthedocs.org/en/latest/sources/examples/