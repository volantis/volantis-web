from flask import Flask, request, render_template, session, redirect
from Match import Match
from AxiomConnection import AxiomConnection
import config
import json
import os

app = Flask(__name__)
app.secret_key = 'super secret key'

proposed_matches = []
proposed_matches.append(Match("3cbc44aa-228f-4df3-b2c2-8f44c201e6e2", "0d0337c1-d4b8-4fa7-a0de-ed64d4372265"))

vouchers = {}

users = {}
for u in config.USERS:
    users[u['id']] = u['email']
    vouchers[u['id']] = []

try:
    c = AxiomConnection()
except:
    print("Something went wrong with connecting to Axiom, quitting!")
    quit()


@app.route('/favicon.ico', methods=["GET"])
def foo():
    return "404"

@app.route('/canihazvoucher')
def canihazvoucher():
    if session.get('id', -1) == -1:
        return redirect('/')
    match = proposed_matches[-1]
    match.next_round()
    if session['id']:
        voucher = c.getVoucher(3, c.login(users[session['id']]))
        vouchers[session['id']].append(voucher)
        return render_template('voucher.html', voucher=voucher)
    else:
        return '403'

@app.route('/game/<sel>', methods=["GET"])
def game_update(sel):
    if session.get('id', -1) == -1:
        return redirect('/')
    match = proposed_matches[-1]
    match.choose(session['id'], int(sel))
    return json.dumps(match.participants)

@app.route('/game/', methods=["GET"])
def game_status():
    if session.get('id', -1) == -1:
        return redirect('/')
    match = proposed_matches[-1]
    return match.status(session['id'])

# @app.route('/discovery', methods=["POST"])
# def discovery():
#     data = request.get_json(force=True)
#     if not data or "id" not in data.keys() or "discovery_id" not in data.keys():
#         return '400'
#     proposed_matches.append(Match(data['id'], data['discovery_id']))
#     return ''


@app.route('/me')
def me():
    return json.dumps({'id' : session['id']})

@app.route('/')
def root():
    return render_template("home.html")

@app.route('/<id>')
def volantis(id):
    # render possible matches
    session['id'] = id
    return render_template('proposed_matches.html',
                           user=str(id),
                           matches=[m for m in proposed_matches if str(id) in m.participants])

@app.route('/<id>/vouchers')
def get_vouchers(id):
    # render possible matches
    return render_template('vouchers.html',
                           vouchers=vouchers[id])

@app.route('/reset')
def reset():
    proposed_matches = []
    session['id'] = 0
    proposed_matches.append(Match("3cbc44aa-228f-4df3-b2c2-8f44c201e6e2", "0d0337c1-d4b8-4fa7-a0de-ed64d4372265"))
    app.secret_key = os.urandom(32)
    return redirect('/')

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
