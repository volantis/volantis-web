# coding: utf-8
from os import path
import json

__config__ = path.abspath(path.join(path.dirname(__file__), 'config.json'))

with open(__config__, 'r') as cf:
    config = json.loads(cf.read())

PW = config.get("password")
INSTITUTION_ID = config.get("institution_id")
INSTITUTION_PW = config.get("institution_password")
USERS = config.get("users")
ACCESS_URL = config.get("merit_access_url")
VOUCHER_URL = config.get("merit_voucher_url")